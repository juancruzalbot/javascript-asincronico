const mostrarPalabras = (param, callback) => {
    let arrayDeCadenas = param.split(" ");
    let i = 0;
    let idInterval = setInterval(
        ()=>{
            console.log(arrayDeCadenas[i++]);
            if (i == arrayDeCadenas.length) {
                clearInterval(idInterval);
                setTimeout(()=>console.log('Cant. de palabas: ' + arrayDeCadenas.length), 9000);
                callback();
            }
        },
        1000
    )
}

const fin = () => console.log('proceso terminado');

mostrarPalabras('hola nada papa', fin);

setTimeout(mostrarPalabras, 3000, 'Hola Nada Pato', fin);

setTimeout(()=>mostrarPalabras('HOLA NADA PATO', fin), 6000);